//
//  ContactViewController.swift
//  GoodAsOldPhones
//
//  Created by Javi Manzano on 03/08/2016.
//  Copyright © 2016 Javi Manzano. All rights reserved.
//

import UIKit

class ContactViewController: UIViewController {

    @IBOutlet weak var scrollView: UIScrollView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.addSubview(scrollView)
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
        scrollView.contentSize = CGSizeMake(375, 800)
    }

    
}
