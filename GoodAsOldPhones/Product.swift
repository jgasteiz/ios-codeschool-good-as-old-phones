//
//  Product.swift
//  GoodAsOldPhones
//
//  Created by Javi Manzano on 03/08/2016.
//  Copyright © 2016 Javi Manzano. All rights reserved.
//

import Foundation

class Product {
    var name: String?
    var productImage: String?
    var cellImage: String?
    var price: Double?
}